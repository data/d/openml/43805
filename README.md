# OpenML dataset: Daily-Electricity-Price-and-Demand-Data

https://www.openml.org/d/43805

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
As of 2020, 6.7 million people reside in Victoria, Australia's second most populated state. Most of them, 5 million, live or work in Melbourne, state's capital. During 2020 Australia was among the first to close international borders, followed by a closure of interstate borders. Victoria introduced some of the strictest pandemic-related restrictions on business activity that resulted in a significant portion of its population working from home.
Description
The dataset covers 2016 days between 1 January 2015 and 6 October 2020. During some intraday intervals RRP was negative, so that energy producers were paying consumers rather than wise versa. Below is a brief description fo the data:

date : datetime, the date of the recording
demand : float, a total daily electricity demand in MWh
RRP : float, a recommended retail price in AUD / MWh
demand_pos_RRP : float, a total daily demand at positive RRP in MWh
RRP_positive : float, an averaged positive RRP, weighted by the corresponding intraday demand in AUD / MWh
demand_neg_RRP : float, an total daily demand at negative RRP in MWh
RRP_negative : float, an average negative RRP, weighted by the corresponding intraday demand in AUD / MWh
frac_at_neg_RRP : float, a fraction of the day when the demand was traded at negative RRP
min_temperature : float, minimum temperature during the day in Celsius
max_temperature : float, maximum temperature during the day in Celsius
solar_exposure : float, total daily sunlight energy in MJ/m2
rainfall : float, daily rainfall in mm
school_day : boolean, if students were at school on that day
holiday : boolean, if the day was a state or national holiday

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43805) of an [OpenML dataset](https://www.openml.org/d/43805). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43805/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43805/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43805/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

